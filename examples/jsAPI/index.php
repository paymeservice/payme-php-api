<?php
/**
 * Created by PhpStorm.
 * User: ika
 * Date: 10/26/15
 * Time: 3:07 AM
 */

include_once('../../lib/PayMe.php');
PayMe::initialize([
    'is_integration' => true,
    'payme_key' => 'DC396889-09AEE642-E6421A2C-1A2CE5A8',
    'payme_secret' => '12345',
]);

if (isset($_POST['payme-buyer-key'])) {
    PayMe::generateSale([
        'sale_price' => rand(1000,9999),
        'sale_buyer_key' => $_POST['payme-buyer-key'],
        'sale_description' => 'Sale through PayMe SDK live ' . rand(1,999999)
    ]);
}
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Payment Test</title>
    <style>
        /* Basic Grey */
        .basic-grey {
            margin-left:auto;
            margin-right:auto;
            max-width: 500px;
            background: #F7F7F7;
            padding: 25px 15px 25px 10px;
            font: 12px Georgia, "Times New Roman", Times, serif;
            color: #888;
            text-shadow: 1px 1px 1px #FFF;
            border:1px solid #E4E4E4;
        }
        .basic-grey h1 {
            font-size: 25px;
            padding: 0px 0px 10px 40px;
            display: block;
            border-bottom:1px solid #E4E4E4;
            margin: -10px -15px 30px -10px;;
            color: #888;
        }
        .basic-grey h1>span {
            display: block;
            font-size: 11px;
        }
        .basic-grey label {
            display: block;
            margin: 0px;
        }
        .basic-grey label>span {
            float: left;
            width: 20%;
            text-align: right;
            padding-right: 10px;
            margin-top: 10px;
            color: #888;
        }
        .basic-grey input[type="text"], .basic-grey input[type="email"], .basic-grey textarea, .basic-grey select {
            border: 1px solid #DADADA;
            color: #888;
            height: 30px;
            margin-bottom: 16px;
            margin-right: 6px;
            margin-top: 2px;
            outline: 0 none;
            padding: 3px 3px 3px 5px;
            width: 70%;
            font-size: 12px;
            line-height:15px;
            box-shadow: inset 0px 1px 4px #ECECEC;
            -moz-box-shadow: inset 0px 1px 4px #ECECEC;
            -webkit-box-shadow: inset 0px 1px 4px #ECECEC;
        }
        .basic-grey textarea{
            padding: 5px 3px 3px 5px;
        }
        .basic-grey select {
            background: #FFF url('down-arrow.png') no-repeat right;
            background: #FFF url('down-arrow.png') no-repeat right);
            appearance:none;
            -webkit-appearance:none;
            -moz-appearance: none;
            text-indent: 0.01px;
            text-overflow: '';
            width: 70%;
            height: 35px;
            line-height: 25px;
        }
        .basic-grey textarea{
            height:100px;
        }
        .basic-grey .button {
            background: #E27575;
            border: none;
            padding: 10px 25px 10px 25px;
            color: #FFF;
            box-shadow: 1px 1px 5px #B6B6B6;
            border-radius: 3px;
            text-shadow: 1px 1px 1px #9E3F3F;
            cursor: pointer;
        }
        .basic-grey .button:hover {
            background: #CF7A7A
        }
        .logo { width: 100%; text-align: center}
        .logo img { width: 400px; }
        .payme-error {
            text-align: center;
            font-weight: bold;
            color: red;
            font-size: 1.2rem;
            font-family: arial;
            margin-bottom: 20px;
            margin-top: -10px;
        }
        * { font-family: arial; }
    </style>
</head>
<body>
<!--<iframe src="http://dev.paymeservice.com/play/ika"></iframe>-->
<div class="logo">
    <img src="http://www.designers-revolution.com/wp-content/uploads/2013/05/online-shop-logo-template-ai-eps-10.jpg" />
</div>

<form name="payment-form" payme-form action="" method="POST" class="basic-grey">
    <h1>Payement Form
        <span>Please fill all the texts in the fields.</span>
    </h1>
    <div payme-error class="payme-error"></div>
    <label>
        <span>First Name :</span>
        <input id="first-name" type="text" name="first-name" payme-first-name value="Ika" />
    </label>

    <label>
        <span>Last Name :</span>
        <input id="last-name" type="text" name="last-name" payme-last-name value="JSTest" />
    </label>

    <label>
        <span>Your Email :</span>
        <input id="email" type="email" name="email" payme-email value="ika@net-comet.com" />
    </label>

    <label>
        <span>Phone Number: </span>
        <input id="phone-number" type="text" name="phone-number" payme-phone value="0502600005" />
    </label>

    <label>
        <span>Social ID</span>
        <input id="social-id" type="text" name="social-id" payme-social-id value="034736637" />
    </label>

    <label>
        <span>Card Number</span>
        <input id="card-number" type="text" name="card-number" payme-card-number value="4580458045804580" />
    </label>

    <label>
        <span>Card Number</span>
        <input id="card-cvv" type="text" name="card-cvv" payme-card-cvv value="123" />
    </label>

    <label>
        <span>Card Number</span>
        <input id="card-exp-month" type="text" name="card-exp-month" payme-card-exp-month value="05" />
    </label>

    <label>
        <span>Card Number</span>
        <input id="card-exp-year" type="text" name="card-exp-year" payme-card-exp-year value="2017" />
    </label>

    <label>
        <span>&nbsp;</span>
        <input type="submit" payme-submit class="button" value="Send" />
    </label>
</form>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="payme.min.js"></script>
<script>
    jQuery(function() {
        payme.initialize({
            seller_payme_id: 'DC396889-09AEE642-E6421A2C-1A2CE5A8',
            is_integration: true,
            log_severity: payme.log_severity.debug,
//            callback: function(buyer_key) {
//                console.log("buyer key is: " + buyer_key + " now do what you want");
//            },
            error_callback: function(err) {
                console.log(err);
            },
            environment_domain: 'dev.paymeservice.com',
            buyer_is_permanent: true,
            nosecure: true
        });
    })
</script>
</body>
</html>
