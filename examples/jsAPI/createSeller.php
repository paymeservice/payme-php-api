<?php
/**
 * Created by PhpStorm.
 * User: ika
 * Date: 10/26/15
 * Time: 3:07 AM
 */

include_once('../../lib/PayMe.php');

if (!function_exists('curl_init')) { die("Critical error: PayMe cannot find CURL installed. Please install curl to use PayMe class\r\n"); }
PayMe::initialize([
    'payme_client_key' => 'omer_sI4vLQW5',
    'payme_client_secret' => '12345',
    'is_integration' => true,
]);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = [];
    foreach ($_POST as $_k => $_v) {
        if (substr($_k,0,4) == "PMF_") $params[str_replace("PMF_", "", $_k)] = $_v;
    }
    $_return = PayMe::createSeller($params);
    var_dump($_return);
}
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Payment Test</title>
    <script src="js/payme.js"></script>
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <style>
        /* Basic Grey */
        .basic-grey {
            margin-left:auto;
            margin-right:auto;
            max-width: 500px;
            background: #F7F7F7;
            padding: 25px 15px 25px 10px;
            font: 12px Georgia, "Times New Roman", Times, serif;
            color: #888;
            text-shadow: 1px 1px 1px #FFF;
            border:1px solid #E4E4E4;
        }
        .basic-grey h1 {
            font-size: 25px;
            padding: 0px 0px 10px 40px;
            display: block;
            border-bottom:1px solid #E4E4E4;
            margin: -10px -15px 30px -10px;;
            color: #888;
        }
        .basic-grey h1>span {
            display: block;
            font-size: 11px;
        }
        .basic-grey label {
            display: block;
            margin: 0px;
        }
        .basic-grey label>span {
            float: left;
            width: 20%;
            text-align: right;
            padding-right: 10px;
            margin-top: 10px;
            color: #888;
        }
        .basic-grey input[type="text"], .basic-grey input[type="email"], .basic-grey textarea, .basic-grey select {
            border: 1px solid #DADADA;
            color: #888;
            height: 30px;
            margin-bottom: 16px;
            margin-right: 6px;
            margin-top: 2px;
            outline: 0 none;
            padding: 3px 3px 3px 5px;
            width: 70%;
            font-size: 12px;
            line-height:15px;
            box-shadow: inset 0px 1px 4px #ECECEC;
            -moz-box-shadow: inset 0px 1px 4px #ECECEC;
            -webkit-box-shadow: inset 0px 1px 4px #ECECEC;
        }
        .basic-grey textarea{
            padding: 5px 3px 3px 5px;
        }
        .basic-grey select {
            background: #FFF url('down-arrow.png') no-repeat right;
            appearance:none;
            -webkit-appearance:none;
            -moz-appearance: none;
            text-indent: 0.01px;
            text-overflow: '';
            width: 70%;
            height: 35px;
            line-height: 25px;
        }
        .basic-grey textarea{
            height:100px;
        }
        .basic-grey .button {
            background: #E27575;
            border: none;
            padding: 10px 25px 10px 25px;
            color: #FFF;
            box-shadow: 1px 1px 5px #B6B6B6;
            border-radius: 3px;
            text-shadow: 1px 1px 1px #9E3F3F;
            cursor: pointer;
        }
        .basic-grey .button:hover {
            background: #CF7A7A
        }
        .logo { width: 100%; text-align: center}
        .logo img { width: 400px; }
        .payme-error {
            text-align: center;
            font-weight: bold;
            color: red;
            font-size: 1.2rem;
            font-family: arial;
            margin-bottom: 20px;
            margin-top: -10px;
        }
        * { font-family: arial; }
    </style>
</head>
<body>
<!--<iframe src="http://dev.paymeservice.com/play/ika"></iframe>-->
<div class="logo">
    <img src="http://www.designers-revolution.com/wp-content/uploads/2013/05/online-shop-logo-template-ai-eps-10.jpg" />
</div>

<form name="payment-form" action="#" method="POST" class="basic-grey">
    <h1>Payement Form
        <span>Please fill all the texts in the fields.</span>
    </h1>
    <div payme-error class="payme-error"></div>
    <label>
        <span>seller_first_name:</span>
        <input id="seller_first_name" type="text" class="disabled" name="PMF_seller_first_name" value="Shimon" />
    </label>
    <label>
        <span>seller_last_name :</span>
        <input id="seller_last_name" type="text" name="PMF_seller_last_name" value="Cohen" />
    </label>

    <label>
        <span>seller_social_id :</span>
        <input id="seller_social_id" type="text" name="PMF_seller_social_id" value="123456782" />
    </label>

    <label>
        <span>seller_birthdate: </span>
        <input id="seller_birthdate" type="text" name="PMF_seller_birthdate" value="19/05/1990" />
    </label>

    <label>
        <span>seller_social_id_issued</span>
        <input id="seller_social_id_issued" type="text" name="PMF_seller_social_id_issued" value="19/05/2008" />
    </label>

    <label>
        <span>seller_gender</span>
        <input id="seller_gender" type="text" name="PMF_seller_gender" value="0" />
    </label>

    <label>
        <span>seller_email</span>
        <input id="seller_email" type="text" name="PMF_seller_email" value="omer+check@paymeservice.com" />
    </label>

    <label>
        <span>seller_phone</span>
        <input id="seller_phone" type="text" name="PMF_seller_phone" value="0503643642" />
    </label>

    <label>
        <span>seller_bank_code</span>
        <input id="seller_bank_code" type="text" name="PMF_seller_bank_code" value="12" />
    </label>

    <label>
        <span>seller_bank_branch</span>
        <input id="seller_bank_branch" type="text" name="PMF_seller_bank_branch" value="354" />
    </label>

    <label>
        <span>seller_bank_account_number</span>
        <input id="seller_bank_account_number" type="text" name="PMF_seller_bank_account_number" value="9320000" />
    </label>

    <label>
        <span>seller_description</span>
        <input id="seller_description" type="text" name="PMF_seller_description" value="MHFC products" />
    </label>

    <label>
        <span>seller_site_url</span>
        <input id="seller_site_url" type="text" name="PMF_seller_site_url" value="www.mss.com" />
    </label>

    <label>
        <span>seller_person_business_type</span>
        <input id="seller_person_business_type" type="text" name="PMF_seller_person_business_type" value="2000" />
    </label>

    <label>
        <span>seller_inc</span>
        <input id="seller_inc" type="text" name="PMF_seller_inc" value="0" />
    </label>



    <label>
        <span>&nbsp;</span>
        <input type="submit" class="button" value="Send" />
    </label>
</form>

</body>
</html>
