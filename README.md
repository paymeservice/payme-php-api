# README #

This repository contains PayMe's PHP API files. 

### What is this repository for? ###

* The repository contains the PHP API SDK to allow online processing with PayMe service.
* In order to know more about PayMe, please visit [PayMe site](https://www.paymeservice.com).

### Initialization ###
* In order to initialize the API SDK, include PayMe.php in your application.
* Initialize PayMe when your page loads using:

Initialization:

    PayMe::initialize([
        'is_integration' => false,
        'payme_key' => 'YOUR-PAYME-KEY-HERE',
        'payme_secret' => 'YOUR-PAYME-SECRET-HERE',
    ]);

### Sale creation ###


    PayMe::generateSale([
        'sale_price' => rand(1000,9999),
        'sale_buyer_key' => $_POST['payme-buyer-key'],
        'sale_description' => 'Sale through PayMe SDK live ' . rand(1,999999)
    ]);

### Need assistance? ###

* Contact support@paymeservice.com