<?php
/**
 * Author: PayMe R&D Department
 * For information on the API parameters, please refer to PayMe API documentation.
 */
class PayMeApiResult {
    var $result = null;
    public static function generateResult($params = []) {
        $_result = new stdClass();
        if (is_array($params)) {
            foreach ($params as $_k => $_v) {
                $_result->$_k = $_v;
            }
        }
        return $_result;
    }
    public static function generateSuccess(PayMeConnector $payme_connector) { //Got HTTP-code 200
        $_result = self::generateResult((array) $payme_connector->getBody());
        $_result->success = true;
        return $_result;
    }
    public static function generateFailure(PayMeConnector $payme_connector) { //Got HTTP-code 500
        $_result = self::generateResult((array) $payme_connector->getBody());
        $_result->success = false;
        return $_result;
    }
    public static function generateError($params = []) {
        $_result = self::generateResult($params);
        $_result->success = false;
        return $_result;
    }
}