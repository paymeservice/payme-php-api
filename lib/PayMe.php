<?php
/**
 * Author: PayMe R&D Department
 * For information on the API parameters, please refer to PayMe API documentation.
 */

const PAYME_PHP_API_VERSION = '0.1.0';
function __autoload($name) { include_once($name . '.php'); }

class PayMe {

    private static $initialized = false;
    private static $config = [
        'is_integration' => false,
        'seller_payme_id' => null,
        'payme_client_secret' => null,
        'payme_client_key' => null
    ];

    private static function setConfig($config) {
        if (is_array($config)) {
            $_config = array_merge(self::$config, $config);
            self::$config = $_config;
        }
    }

    public static function initialize($config) {
        self::setConfig($config);
        self::$initialized = true;
    }

    public static function addCredentials($sale_details, $target) { // 0 =  merchant, 1 = seller
        switch ($target) {
            case 0: {
                if (isset(self::$config['payme_client_key']) && isset(self::$config['payme_client_secret'])) {
                    $sale_details['payme_client_key'] = self::$config['payme_client_key'];
                    $sale_details['payme_client_secret'] = self::$config['payme_client_secret'];
                    return $sale_details;
                }
                die("Critical error: PayMe credentials are not set\r\n");
            }
            case 1: {
                if (isset(self::$config['seller_payme_id'])) {
                    $sale_details['seller_payme_id'] = self::$config['seller_payme_id'];
                    return $sale_details;
                }
                die("Critical error: PayMe credentials are not set\r\n");
            }
            case 2: {
                if (isset(self::$config['seller_payme_id'])) {
                    $sale_details['seller_payme_id'] = self::$config['seller_payme_id'];
                    return $sale_details;
                }
                if (isset(self::$config['payme_client_key']) && isset(self::$config['payme_client_secret'])) {
                    $sale_details['payme_client_key'] = self::$config['payme_client_key'];
                    $sale_details['payme_client_secret'] = self::$config['payme_client_secret'];
                    return $sale_details;
                }
                die("Critical error: PayMe credentials are not set\r\n");
            }
        }
    }

    public static function generateUrl($url = "/") {
        if ( (isset(self::$config['is_integration'])) && (self::$config['is_integration'] === true) )   { return "https://preprod.paymeservice.com" . $url; }
        return "https://ng.paymeservice.com" . $url;
    }

    public static function validateParams($params,$req_objects) {
        if (isset($params) && isset($req_objects)) {
            $_result = [];
            foreach ($req_objects as $x) {
                if (!isset($params[$x])) {
                    $_result["error_code"] = 1;
                    $_result["status_error_details"] = "Required object is missing: " . $x;
                    $_result["success"] = false;
                    return PayMeApiResult::generateError($_result);
                }
            }
            return null;
        }
    }

    public static function createRequest($_url, $params) {
        $_pc = new PayMeConnector();
        $_pc->fetch($_url, $params);
        if ($_pc->validateHttpSuccess()) {
            return PayMeApiResult::generateSuccess($_pc);
        }
        if ($_pc->validateHttpFailure()) {
            return PayMeApiResult::generateFailure($_pc);
        }
        return PayMeApiResult::generateResult(["success"=>false, "error_code"=>1, "status_error_details"=>"Bad URL"]);
    }
    /* API Functions */

    /**
     * generateSale - generate a new sale in PayMe
     * @param array $sale_details
     * @return PayMeApiResult
     */

    public static function generateSale($sale_details = []) {
        $sale_details = self::addCredentials($sale_details, 1);
        $_url = PayMe::generateUrl("/api/generate-sale");
        $req_objects = ["sale_price", "product_name", "installments"];

        $result = self::validateParams($sale_details, $req_objects);
        if ($result) {
            return $result;
        }
        return self::createRequest($_url, $sale_details);
    }

    public static function paySale($sale_details = []) {
        $sale_details = self::addCredentials($sale_details, 1);
        $_url = PayMe::generateUrl("/api/pay-sale");
        $req_objects = ["credit_card_number", "credit_card_cvv", "credit_card_exp", "buyer_social_id", "buyer_email", "payme_sale_id", "buyer_name"];

        $result = self::validateParams($sale_details, $req_objects);
        if ($result) {
            return $result;
        }
        return self::createRequest($_url, $sale_details);
    }

    public static function generateSubscription($sale_details = []) {
        $sale_details = self::addCredentials($sale_details, 1);
        $_url = PayMe::generateUrl("/api/generate-subscription");
        $req_objects = ["sub_iteration_type", "sub_description", "sub_price"];

        $result = self::validateParams($sale_details, $req_objects);
        if ($result) {
            return $result;
        }
        return self::createRequest($_url, $sale_details);
    }
    public static function refundSale($sale_details = []) {
        $sale_details = self::addCredentials($sale_details, 2);
        $_url = PayMe::generateUrl("/api/refund-sale");
        $req_objects = [];

        $result = self::validateParams($sale_details, $req_objects);
        if ($result) {
            return $result;
        }
        return self::createRequest($_url, $sale_details);
    }

    public static function createSeller($sale_details = []) {
        $sale_details = self::addCredentials($sale_details, 0);
        $_url = PayMe::generateUrl("/api/create-seller");
        $req_objects = ["seller_first_name", "seller_last_name", "seller_social_id", "seller_birthdate", "seller_social_id_issued", "seller_gender", "seller_email", "seller_phone", "seller_bank_code", "seller_bank_branch", "seller_bank_account_number", "seller_description", "seller_site_url", "seller_person_business_type", "seller_inc"];

        $result = self::validateParams($sale_details, $req_objects);
        if ($result) {
            return $result;
        }
        return self::createRequest($_url, $sale_details);
    }

    public static function verifyTransaction($sale_details = []) {
        $sale_details = self::addCredentials($sale_details, 0);
        $_url = PayMe::generateUrl("/api/verify-transaction");
        $req_objects = ["seller_payme_id", "payme_transaction_id"];

        $result = self::validateParams($sale_details, $req_objects);
        if ($result) {
            return $result;
        }
        return self::createRequest($_url, $sale_details);
    }

    public static function withdrawBalance($sale_details = []) {
        $sale_details = self::addCredentials($sale_details, 0);
        $_url = PayMe::generateUrl("/api/withdraw-balance");
        $req_objects = ["payme_client_key", "seller_payme_id", "withdrawal_currency"];

        $result = self::validateParams($sale_details, $req_objects);
        if ($result) {
            return $result;
        }
        return self::createRequest($_url, $sale_details);
    }

    public static function getSellers($sale_details = []) {
        $sale_details = self::addCredentials($sale_details, 2);
        $_url = PayMe::generateUrl("/api/get-sellers");
        $req_objects = [];

        $result = self::validateParams($sale_details, $req_objects);
        if ($result) {
            return $result;
        }
        return self::createRequest($_url, $sale_details);
    }

    public static function getSales($sale_details = []) {
        $sale_details = self::addCredentials($sale_details, 2);
        $_url = PayMe::generateUrl("/api/get-sales");
        $req_objects = [];

        $result = self::validateParams($sale_details, $req_objects);
        if ($result) {
            return $result;
        }
        return self::createRequest($_url, $sale_details);
    }

    public static function getSubscriptions($sale_details = []) {
        $sale_details = self::addCredentials($sale_details, 2);
        $_url = PayMe::generateUrl("/api/get-subscriptions");
        $req_objects = [];

        $result = self::validateParams($sale_details, $req_objects);
        if ($result) {
            return $result;
        }
        return self::createRequest($_url, $sale_details);
    }

    public static function getFinancialTransactions($sale_details = []) {
        $sale_details = self::addCredentials($sale_details, 2);
        $_url = PayMe::generateUrl("/api/get-financial-transactions");
        $req_objects = [];

        $result = self::validateParams($sale_details, $req_objects);
        if ($result) {
            return $result;
        }
        return self::createRequest($_url, $sale_details);
    }
}