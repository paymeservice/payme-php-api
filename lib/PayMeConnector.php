

<?php
/**
 * Author: PayMe R&D Department
 * For information on the API parameters, please refer to PayMe API documentation.
 */
class PayMeConnector {

    const HTTP_SUCCESS = 200;
    const HTTP_FAILURE = 500;

    var $body = [];
    var $headers = null;
    private $handler; // Handle
    private $options = [
        CURLOPT_USERAGENT         => 'PayMe PHP API v',
        CURLOPT_ENCODING          => 'gzip,deflate',
        CURLOPT_TIMEOUT           => 30,
        CURLOPT_RETURNTRANSFER    => 1,
        CURLOPT_MAXREDIRS         => 10,
        CURLOPT_HTTP_VERSION      => CURL_HTTP_VERSION_1_1,
    ];

    /* Numeric keys fix - ArrayB overrides arrayA */
    static function mergeArrays($arrayA, $arrayB) {
        foreach ($arrayB as $_k => $_v) { $arrayA[$_k] = $_v; }
        return $arrayA;
    }

    public static function clearEmptyFields($array) {
        foreach ($array as $_key => $_value) {
            if (empty($_value)) {
                unset($array[$_key]);
            }
        }
        return $array;
    }

    public function getBody() {
        return $this->body;
    }

    public function fetch($url, $post_data = [], $options = []) {
        if (!function_exists('curl_init')) { die("Critical error: PayMe cannot find CURL installed. Please install curl to use PayMe class\r\n"); }
        $this->handler = curl_init($url);
        curl_setopt_array($this->handler, $this->mergeArrays($this->options, $options));
        if (is_array($post_data) && count($post_data) > 0) {
            $post_data = PayMeConnector::clearEmptyFields($post_data);
            curl_setopt($this->handler, CURLOPT_POST, count($post_data));
            curl_setopt($this->handler, CURLOPT_POSTFIELDS, json_encode($post_data));
            curl_setopt($this->handler, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($this->handler, CURLOPT_HTTPHEADER, [
                "Content-Type: application/json; charset=utf-8",
                "Accept:application/json, text/javascript, */*; q=0.01",
                "cache-control: no-cache",
            ]);
        }

        $_response = curl_exec($this->handler);
        if(curl_error($this->handler)) {
            echo 'Curl error: ' . curl_error($this->handler);
            return;
        }

        $this->headers = curl_getinfo($this->handler);
        $this->body = json_decode(substr($_response, 0, curl_getinfo($this->handler, CURLINFO_HEADER_SIZE)));
        if (!isset($this->body)) $this->body = json_decode($_response);
        curl_close($this->handler);
    }

    public function getHttpCode() { return $this->headers['http_code']; }
    public function validateHttpSuccess() { return $this->getHttpCode() == self::HTTP_SUCCESS; }
    public function validateHttpFailure() { return $this->getHttpCode() == self::HTTP_FAILURE; }
}